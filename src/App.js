import React, {useEffect, useState} from 'react';
import './App.css';

const App = () => {

    const [datas, setDatas] = useState([]);

    useEffect(() => {

        fetch(`https://ofelia-server.herokuapp.com/products`)
            .then(response => response.json())
            .then(data => {
                setDatas(data)
            });
    });

    return (
        <div className="App">
            <div>
                {/* Products */}
                <div className="view-item">
                    <h6>MOST LOVED COLLECTION</h6>
                    <h4>MODERN MATTE LIPSTICK</h4>

                </div>
                {/*  */}
            </div>
            <main>
                {
                    datas.map(product => (
                            <div className="article" id={"item"} key={product.id}>
                                <a>
                                    <div className="img-item1">
                                        <img src={product.src} alt={product.name} />
                                    </div>
                                    <div className="img-item2">
                                        <img src={"https://product.hstatic.net/1000318088/product/im-blushing-1_256810e97e9442068bb91886d6613a4f_large.jpg"} alt={product.name} />
                                    </div>
                                </a>
                                <div className="info-item">
                                    <h3>{product.name}</h3>
                                    <p>
                                        {parseInt(product.price).toLocaleString()}
                                        VNĐ
                                    </p>
                                </div>
                            </div>

                        )
                    )
                }
            </main>
        </div>
    );
};

export default App;
